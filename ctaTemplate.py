# encoding: UTF-8

'''
本文件包含了CTA引擎中的策略开发用模板，开发策略时需要继承CtaTemplate类。
'''

from ctaBase import *
from vtConstant import *


########################################################################
class CtaTemplate(object):
    """CTA策略模板"""
    
    # 策略类的名称和作者
    className = 'CtaTemplate'
    author = EMPTY_UNICODE
    
    
    # 策略的基本参数
    name = EMPTY_UNICODE           # 策略实例名称
    vtSymbol = EMPTY_STRING        # 交易的合约vt系统代码    
    
    # 策略的基本变量，由引擎管理
    #inited = False                 # 是否进行了初始化
    #trading = False                # 是否启动交易，由引擎管理
    pos = 0                        # 持仓情况
    
    # 参数列表，保存了参数的名称
    paramList = ['name',
                 'className',
                 'author',
                 'vtSymbol']
    
    # 变量列表，保存了变量的名称
    varList = ['pos']

    #----------------------------------------------------------------------
    def __init__(self, ctaEngine):
        """Constructor"""
        self.ctaEngine = ctaEngine

    
    #----------------------------------------------------------------------
    def onInit(self):
        """初始化策略（必须由用户继承实现）"""
        raise NotImplementedError
    
    #----------------------------------------------------------------------
    def onStart(self):
        """启动策略（必须由用户继承实现）"""
        raise NotImplementedError
    
    #----------------------------------------------------------------------
    def onStop(self):
        """停止策略（必须由用户继承实现）"""
        raise NotImplementedError

    #----------------------------------------------------------------------
    def onTick(self, tick):
        """收到行情TICK推送（必须由用户继承实现）"""
        raise NotImplementedError

    #----------------------------------------------------------------------
    def onOrder(self, order):
        """收到委托变化推送（必须由用户继承实现）"""
        raise NotImplementedError
    
    #----------------------------------------------------------------------
    def onTrade(self, trade):
        """收到成交推送（必须由用户继承实现）"""
        raise NotImplementedError
    
    #----------------------------------------------------------------------
    def onBar(self, bar):
        """收到Bar推送（必须由用户继承实现）"""
        raise NotImplementedError
    
    #----------------------------------------------------------------------
    def buy(self, price, volume):
        """买开"""
        return self.sendOrder(CTAORDER_BUY, price, volume)
    
    #----------------------------------------------------------------------
    def sell(self, price, volume):
        """卖平"""
        return self.sendOrder(CTAORDER_SELL, price, volume)       

    #----------------------------------------------------------------------
    def short(self, price, volume):
        """卖开"""
        return self.sendOrder(CTAORDER_SHORT, price, volume)          
 
    #----------------------------------------------------------------------
    def cover(self, price, volume):
        """买平"""
        return self.sendOrder(CTAORDER_COVER, price, volume)
        
    #----------------------------------------------------------------------
    def sendOrder(self, orderType, price, volume):
        """发送委托"""
        vtOrderID = self.ctaEngine.sendOrder(self.vtSymbol, orderType, 
                                              price, volume, self) 
        return vtOrderID
        

    
    
